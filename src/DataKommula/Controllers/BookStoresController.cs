using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataKommula.Models;

namespace DataKommula.Controllers
{
    public class BookStoresController : Controller
    {
        private AppDbContext _context;

        public BookStoresController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: BookStores
        public IActionResult Index()
        {
            return View(_context.BookStores.ToList());
        }

        // GET: BookStores/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);
            if (bookStore == null)
            {
                return HttpNotFound();
            }

            return View(bookStore);
        }

        // GET: BookStores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BookStores/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BookStore bookStore)
        {
            if (ModelState.IsValid)
            {
                _context.BookStores.Add(bookStore);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookStore);
        }

        // GET: BookStores/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);
            if (bookStore == null)
            {
                return HttpNotFound();
            }
            return View(bookStore);
        }

        // POST: BookStores/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BookStore bookStore)
        {
            if (ModelState.IsValid)
            {
                _context.Update(bookStore);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookStore);
        }

        // GET: BookStores/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);
            if (bookStore == null)
            {
                return HttpNotFound();
            }

            return View(bookStore);
        }

        // POST: BookStores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            BookStore bookStore = _context.BookStores.Single(m => m.BookStoreId == id);
            _context.BookStores.Remove(bookStore);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
