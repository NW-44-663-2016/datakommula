﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataKommula.Models
{
    public class BookStore
    {


        public int BookStoreId { get; set; }

        public string StoreName { get; set; }


        public string ManagerName { get; set; }


        public int establishedyear { get; set; }
        public int LocationID { get; set; }
    }
}
